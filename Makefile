
.DEFAULT_GOAL := help

TOPDIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

ANSIBLE_PLAYBOOK_BIN ?= ansible-playbook
ANSIBLE_PARAMS_ADD ?= -vv

ifdef DEBUG
	ECHO := @echo
else
	ECHO := 
endif

include Makefile.portability


# targets without a comment after ".PHONY: target" are not listed in help
help:
	@echo Env variables:
	@echo    DEBUG value: $(DEBUG)
	@echo    ANSIBLE_PARAMS_ADD value: $(ANSIBLE_PARAMS_ADD)
	@echo    ANSIBLE_PLAYBOOK_BIN value: $(ANSIBLE_PLAYBOOK_BIN)
	@echo
	@echo Available targets:
	@echo
ifdef HAS_COLUMN
ifdef COLUMN_HAS_TABLE_WRAP
	@cat Makefile* | grep '^.PHONY: .* #' | sed 's/\.PHONY: \(.*\) # \(.*\)/\1|\2/' | column -t -s '|' -W 2 $(SORTED_OUTPUT)
else
	@cat Makefile* | grep '^.PHONY: .* #' | sed 's/\.PHONY: \(.*\) # \(.*\)/\1|\2/' | column -t -s '|' $(SORTED_OUTPUT)
endif
else
	@cat Makefile* | grep '^.PHONY: .* #' | sed 's/\.PHONY: \(.*\) # \(.*\)/\1 - \2/' $(SORTED_OUTPUT)
endif
.PHONY: help # this text


ANSIBLE_VERIFY_PLAYBOOKS :=
ANSIBLE_VERIFY_PLAYBOOKS += verify-deluge

ANSIBLE_SETUP_PLAYBOOKS :=
ANSIBLE_SETUP_PLAYBOOKS += deluge
ANSIBLE_SETUP_PLAYBOOKS += test-pi-container

ANSIBLE_PLAYBOOKS := $(ANSIBLE_VERIFY_PLAYBOOKS) $(ANSIBLE_SETUP_PLAYBOOKS)
$(ANSIBLE_PLAYBOOKS):
	@$(ECHO) $(ANSIBLE_PLAYBOOK_BIN) $(ANSIBLE_PARAMS_ADD) $(TOPDIR)/playbooks/$@.yml
	@echo '$(shell date) Makefile: success: $@ '
.PHONY: $(ANSIBLE_PLAYBOOKS)

test-pi-start: ANSIBLE_PARAMS_ADD += -e desired_state=started
test-pi-start: test-pi-container
.PHONY: test-pi-start  # start test-pi container

test-pi-stop: ANSIBLE_PARAMS_ADD += -e desired_state=absent
test-pi-stop: test-pi-container
.PHONY: test-pi-stop  # stop test-pi container

all: deluge
	@echo $(shell date) Makefile: success: $@
.PHONY: all  #  deluge

all-testpi: test-pi-start | all-test2
	# $(MAKE) all ANSIBLE_PARAMS_ADD='-l test-pi'
	@echo $(shell date) Makefile: success: $@
.PHONY: all-testpi  #  run targets against test-pi container

# helper target, it IS needed.
all-test2: ANSIBLE_PARAMS_ADD := -l test-pi
all-test2: all

verify: $(ANSIBLE_VERIFY_PLAYBOOKS)
	@echo $(shell date) Makefile: success: $@
.PHONY: verify #  deluge

verify-testpi: test-pi-start | verify-testpi-helper
	@echo $(shell date) Makefile: success: $@
.PHONY: verify-testpi #  verify against test-pi container

verify-testpi-helper: ANSIBLE_PARAMS_ADD := -l test-pi
verify-testpi-helper: verify

ansible-lint:
	@ansible-lint --version
	@ansible-lint -v -p
.PHONY: ansible-lint  # run ansible-lint

docker-ansible-lint:
	docker run -v $(TOPDIR):/src yokogawa/ansible-lint ansible-lint -v -p /src
.PHONY: docker-ansible-lint  # ansible-lint in a docker
